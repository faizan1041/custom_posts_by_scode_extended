<?php

/*
Plugin Name: Custom Posts by Shortcode Search
Plugin URI: http://faizan-ali.com/
Description: [searchposts post_type=""]
Version: 1.0.1
Author: Faizan Ali
Author URI: http://faizan-ali.com/
*/



    
    // Add Shortcode
function searchposts_shortcode( $atts ) {
    
     if(isset($_POST['searchposts'])) {
    
        // var_dump($_POST['cat-parent']);
        // var_dump($_POST['cat-child']);
	// Attributes
	extract( shortcode_atts(
		array(
			'post_type' => 'documents',
		), $atts )
	);
        

$cpss_taxonomy = $_POST['taxonomy'];
 
$cpss_terms = array($_POST['cat-parent'],$_POST['cat-child']);

$cpss_s = $_POST['searchposts'];



        // WP_Query arguments
$args = array (
	'post_type'              => 'documents',
	's'                      => $cpss_s,
	'order'                  => 'DESC',
	'orderby'                => 'date',
        'tax_query' => array(
		array(
			'taxonomy' => $cpss_taxonomy,
			'field'    => 'slug',
			'terms'    => $cpss_terms,
		),
	),
);




// The Query
$newquery = new WP_Query( $args );
$cust_posts = '<div class="gmw-posts-wrapper">';
// The Loop
if ( $newquery->have_posts() ) {
	while ( $newquery->have_posts() ) {
		$newquery->the_post();
		// do something
      
      $cust_posts .=  '<div class="wppl-title-holder">
                        <h2 class="wppl-h2">
                        <a href="';
      
      $cust_posts .= get_permalink() . '">' . get_the_title() . '</a>';
      $cust_posts .= '</h2>
                </div>';
      $cust_posts .= '<div id="wppl-thumb" class="wppl-thumb"><a href="' . get_permalink() . '">' .  get_the_post_thumbnail() . '</a>';
      $cust_posts .= '</div>
                <div class="wppl-excerpt">' . get_the_excerpt() . '</div>';
      
                
	}
} else {
	// no posts found
    $cust_posts .= 'no posts found';
}



// Restore original Post Data
wp_reset_postdata();


$cust_posts .= '</div>';


return $cust_posts;
        
        
}
        
}
add_shortcode( 'searchposts', 'searchposts_shortcode' );    




  // Add Shortcode
function searchposts_form_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'post_type' => 'documents',
		), $atts )
	);
  
        
        
      $term_parents = cpss_get_tax_parents($atts['post_type']);
      $term_children = cpss_get_tax_children($atts['post_type']);
 /*     $term_parents_name = $term_parents['name'];
      $term_parents_slug = $term_parents['slug'];
      $term_children_name = $term_children['name'];
      $term_children_slug = $term_children['slug']; */



$searchform = '

<form  method="post" action="">
				
			<input id="element_1" name="searchposts" class="element text medium" type="text" maxlength="255" value=""> 
		
		
		
		<select id=""  name="cat-parent">
                <option value="" selected="selected"></option>';
foreach ($term_parents as  $term_parent){

$searchform .= '<option  value="'. $term_parent['slug'] .'">' . $term_parent['name'] . '</option>';
}

$searchform .= '


		</select>
		
		<select   name="cat-child"> 
			<option value="" selected="selected"></option> ';

foreach ($term_children as  $term_child){
$searchform .= '<option  class="exampleSubselect'.$term_child['parent_terms'].'" value="'. $term_child['slug']  .'">'. $term_child['name'] .'</option>';


}
$searchform .= '</select> 
		
			
			  
			    <input type="hidden" name="taxonomy" value="'.$term_parents[0]['taxonomy'].'">
				<input class="button_text" type="submit" name="submit" value="Submit">
		
		</form>';
        


return $searchform;
        
}
add_shortcode( 'searchpostsform', 'searchposts_form_shortcode' );    




function cpss_get_tax_parents($atts){
     //get taxonomy objects for the post type $atts
        $taxonomy_names = get_object_taxonomies( $atts);
        foreach ($taxonomy_names as $taxonomy_name) {
           if($taxonomy_name != 'post_tag') {
            //get the terms associated with the category
               $args = array(
                    'orderby'           => 'name', 
                    'order'             => 'ASC',
                    'hide_empty'        => true, 
                    'exclude'           => array(), 
                    'exclude_tree'      => array(), 
                    'include'           => array(),
                    'number'            => '', 
                    'fields'            => 'all', 
                    'slug'              => '', 
                    'parent'            => 0,  // shows all parents.. 
                    'hierarchical'      => true, 
                    'child_of'          => 0, 
                    'get'               => '', 
                    'name__like'        => '',
                    'description__like' => '',
                    'pad_counts'        => false, 
                    'offset'            => '', 
                    'search'            => '', 
                    'cache_domain'      => 'core'
                ); 
               $terms = get_terms($taxonomy_name, $args);
               
               foreach ($terms as $term){
                    if(!in_array($term->name,$term_list)){
                         $term_list[] = array(
                             'name' => $term->name, 'slug' => $term->slug, 
                             'taxonomy' => $taxonomy_name, 
                                 );
                        }
                   
               } //ends foreach
               
               
               
               
           }
          } //end foreach
         
          return $term_list;
          
}



function cpss_get_tax_children($atts){
          //get taxonomy objects for the post type $atts
        $taxonomy_names = get_object_taxonomies( $atts);
        foreach ($taxonomy_names as $taxonomy_name) {
           if($taxonomy_name != 'post_tag') {
            //get the terms associated with the category
               $args = array(
                    'orderby'           => 'name', 
                    'order'             => 'ASC',
                   
                    
                   
                ); 
               $terms = get_terms($taxonomy_name, $args);
               
               foreach ($terms as $term){
                   if(is_parent_tax($term)==FALSE){
                    $parent_terms = cpss_get_cat_parents($term->term_id);
                    if(!in_array($term->name,$term_list)){
                         $term_list[] = array(
                             'name' => $term->name, 'slug' => $term->slug, 
                             'taxonomy' => $taxonomy_name, 'parent_terms' =>$parent_terms,
                                 );
                        }//endif
                   }//endif
                   
               } //ends foreach
               
               
               
               
           }
          } //end foreach
         
          return $term_list;
          
}




function is_parent_tax($term){
    if($term->parent == 0){
        return TRUE;
    }
    
    else {
        return FALSE;
    }
}  



function cpss_scripts(){
     wp_register_script('cpss-menu-toggle', plugin_dir_url(__FILE__). 'cpss-menu-toggle.js');
     wp_enqueue_script('cpss-menu-toggle');
     
     wp_register_style('cpss_menu-toggle-css', plugin_dir_url(__FILE__). 'cpss-menu-toggle.css');
     wp_enqueue_style('cpss_menu-toggle-css');
}

add_action( 'wp_enqueue_scripts', 'cpss_scripts' );              


function cpss_get_cat_parents($term){
    //Returns a list of the parents of a category, including the category, in hierarchical order.
    $parent_terms = get_category_parents($term);
    $parent_terms = explode('/', $parent_terms);
    
    foreach ($parent_terms as $parent_term){
        $parent_term = strtolower($parent_term);
        $parent_term_mod .= ' example-' . str_replace(' ', '-', $parent_term);
    }
    
    return $parent_term_mod;
}